imperial_officer_common = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "technical_console_schematic_1", weight = 2475000},
		{itemTemplate = "technical_console_schematic_2", weight = 2475000},
		{itemTemplate = "couch_blue_schematic", weight = 2475000},
		{itemTemplate = "painting_vader_victory", weight = 2475000},
		{groupTemplate = "imperial_books", weight = 100000},
	}
}

addLootGroupTemplate("imperial_officer_common", imperial_officer_common)
