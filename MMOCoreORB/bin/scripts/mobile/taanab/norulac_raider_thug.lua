norulac_raider_thug = Creature:new {
	objectName = "@mob/creature_names:norulac_raider_thug",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	mobType = MOB_NPC,
	socialGroup = "norulac",
	faction = "norulac_raiders",
	level = 125,
	chanceHit = 1.9,
	damageMin = 950,
	damageMax = 1390,
	baseXp = 10500,
	baseHAM = 35000,
	baseHAMmax = 49000,
	armor = 1,
	resists = {55,55,60,60,70,75,55,75,55},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mercenary_warlord_hum_m.iff",
		"object/mobile/dressed_mercenary_warlord_nikto_m.iff",
		"object/mobile/dressed_mercenary_warlord_wee_m.iff"
	},

	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 2500000},
				{group = "pistols", chance = 2000000},
				{group = "rifles", chance = 2000000},
				{group = "carbines", chance = 2000000},
				{group = "melee_weapons", chance = 750000},
				{group = "clothing_attachments", chance = 750000}
			}
		}
	},


	-- Primary and secondary weapon should be different types (rifle/carbine, carbine/pistol, rifle/unarmed, etc)
	-- Unarmed should be put on secondary unless the mobile doesn't use weapons, in which case "unarmed" should be put primary and "none" as secondary
	primaryWeapon = "rotary_weapons",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	
	-- primaryAttacks and secondaryAttacks should be separate skill groups specific to the weapon type listed in primaryWeapon and secondaryWeapon
	-- Use merge() to merge groups in creatureskills.lua together. If a weapon is set to "none", set the attacks variable to empty brackets
	primaryAttacks = merge(rotarywielder),
	secondaryAttacks = merge(tkamaster),
}

CreatureTemplates:addCreatureTemplate(norulac_raider_thug, "norulac_raider_thug")
