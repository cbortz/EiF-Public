jessik_outfit = {
	{ 
		{objectTemplate = "object/tangible/wearables/jacket/jacket_s21.iff", customizationVariables = {{"/private/index_color_1", 181}, {"/private/index_color_2", 181}} },
		{objectTemplate = "object/tangible/wearables/armor/ubese/armor_ubese_shirt.iff", customizationVariables = {{"/private/index_color_1", 6}} },
		{objectTemplate = "object/tangible/wearables/hat/hat_s14.iff", customizationVariables = {{"/private/index_color_1", 160}} },
		{objectTemplate = "object/tangible/wearables/pants/pants_s05.iff", customizationVariables = {{"/private/index_color_1", 6}, {"/private/index_color_2", 254}} },
		{objectTemplate = "object/tangible/wearables/boots/boots_s14.iff", customizationVariables = {{"/private/index_color_1", 6}} },
		
	}
	 
}

addOutfitGroup("jessik_outfit", jessik_outfit)
