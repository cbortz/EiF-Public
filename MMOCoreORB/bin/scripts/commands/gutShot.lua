--EiF 2022

GutShotCommand = {
    name = "gutshot",

	damageMultiplier = 4.0,
	speedMultiplier = 1.5,
	healthCostMultiplier = 0,
	actionCostMultiplier = 35,
	mindCostMultiplier = 0,
        accuracyBonus = 15,

	poolsToDamage = RANDOM_ATTRIBUTE,

	coneAngle = 30,
	coneAction = true,

	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "gutshot",

	dotEffects = {
	  DotEffect(
		BLEEDING,
		{ "resistance_bleeding", "bleed_resist" },
		HEALTH,
    true,
		0,
		100,
		60,
		12,
		200
	  )
	},

	weaponType = SHOTGUNWEAPON,

	range = -1
}

AddCommand(GutShotCommand)
