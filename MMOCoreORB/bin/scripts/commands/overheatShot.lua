--EiF 2022

OverheatShotCommand = {
        name = "overheatshot",

	damageMultiplier = 5.5,
	speedMultiplier = 2.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 50,
        accuracyBonus = 15,

	poolsToDamage = RANDOM_ATTRIBUTE,

	coneAngle = 30,
	coneAction = true,

	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "overheatshot",

	dotEffects = {
	  DotEffect(
		ONFIRE,
		{ "resistance_fire", "fire_resist" },
		ATTACK_POOL,
		true,
		0,
		75,
		30,
		15,
		25,
		10
	  )
	},

	weaponType = SHOTGUNWEAPON,

	range = -1
}

AddCommand(OverheatShotCommand)
