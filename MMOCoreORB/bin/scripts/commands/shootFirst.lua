--EiF 2022

ShootFirstCommand = {
        name = "shootfirst",

  damageMultiplier = 4.0,
	speedMultiplier = 0.5,
	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 10,
  accuracyBonus = 5,

  stateEffects = {
	  StateEffect(
		STUN_EFFECT,
		{},
		{ "stun_defense" },
		{ "jedi_state_defense", "resistance_states" },
		100,
		0,
		60
	  )
	},

	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "shootfirst",


	range = -1
}

AddCommand(ShootFirstCommand)
