--Copyright (C) 2020 <SWG EiF>


--This File is part of SWG: Empire in Flames. It is not designed to be redistributed or used in other SWGEMU based emulators or any other emulators.
--If you wish to use this file, please contact the Empire in Flames team for permission to do so.
--This software is distributed without any warranty; without an impied warranty of merchantability or fitness for a particular purpose.


object_tangible_wearables_base_base_cybernetic_torso = object_tangible_wearables_base_shared_base_cybernetic_torso:new {
}

ObjectTemplates:addTemplate(object_tangible_wearables_base_base_cybernetic_torso, "object/tangible/wearables/base/base_cybernetic_torso.iff")
