/*
 * DyeToolTemplate.h
 *
 *  Created on: Jun 23, 2021
 *      Author: Phoenix
 */

#ifndef DYETOOLTEMPLATE_H_
#define DYETOOLTEMPLATE_H_

#include "templates/SharedTangibleObjectTemplate.h"

class DyeToolTemplate : public SharedTangibleObjectTemplate {
	uint32 mask;

public:
	DyeToolTemplate() {
		mask = 0;
	}

	~DyeToolTemplate() {
	}

	void readObject(LuaObject* templateData) override {
		SharedTangibleObjectTemplate::readObject(templateData);
		mask = templateData->getIntField("canDyeType");
	}

	uint32 getDyeType() const {
		return mask;
	}

	bool isDyeToolTemplate() const {
		return true;
	}
};

#endif /* DYETOOLTEMPLATE_H_ */
