/*
 * UpdatePopulationTask.h
 *
 *  Created on: 22/04/2022
 *      Author: Halyn
 */

#ifndef UPDATEPOPULATIONTASK_H_
#define UPDATEPOPULATIONTASK_H_

#include "server/ServerCore.h"
#include "server/zone/ZoneServer.h"

class UpdatePopulationTask : public Task {
	ManagedReference<ZoneServer*> zoneServer;
public:
	UpdatePopulationTask(ZoneServer* srv) {
		zoneServer = srv;
	}

	void run() {
		int onlinePlayers = zoneServer->getConnectionCount();

		int galaxyID = zoneServer->getGalaxyID();

		StringBuffer query;
		query << "Update `swgemu`.`galaxy` SET `population`= " + String::valueOf(onlinePlayers) + " WHERE `galaxy_id`= " + String::valueOf(galaxyID);

		try {
			ServerDatabase::instance()->executeStatement(query);
		}
		catch (const DatabaseException& e) {
			Logger::console.error(e.getMessage());
		}
		query.deleteAll();

		schedule(5 * 60 * 1000);
	}
};


#endif /* UPDATEPOPULATIONTASK_H_ */
