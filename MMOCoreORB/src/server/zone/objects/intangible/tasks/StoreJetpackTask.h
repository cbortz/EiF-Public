/*
 * StoreJetpackTask.h
 *
 *  Created on: 5/1/2022
 *      Author: Halyn
 */

#ifndef STOREJETPACKTASK_H_
#define STOREJETPACKTASK_H_

#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/intangible/ControlDevice.h"
#include "server/zone/objects/intangible/VehicleControlDevice.h"

class StoreJetpackTask : public Task {
	ManagedReference<CreatureObject*> player;
	ManagedReference<VehicleControlDevice*> device;
	String taskName;

public:
	StoreJetpackTask(VehicleControlDevice* controlDevice, CreatureObject* creo, const String& task) {
		player = creo;
		device = controlDevice;
		taskName = task;
	}

	void run() {

		Locker locker(player);

		player->removePendingTask("store_jetpack");
		player->updateCooldownTimer("mount_dismount", 0);

		ManagedReference<TangibleObject*> vehicle = device->getControlledObject();

		if (vehicle == nullptr)
			return;

		if (player->isRidingMount())
			player->executeObjectControllerAction(STRING_HASHCODE("dismount"));

		Locker crossLocker(vehicle, player);

		Reference<Task*> decayTask = vehicle->getPendingTask("decay");

		if (decayTask != nullptr) {
			decayTask->cancel();
			vehicle->removePendingTask("decay");
		}

		vehicle->destroyObjectFromWorld(true);

		if (vehicle->isCreatureObject())
			(cast<CreatureObject*>(vehicle.get()))->setCreatureLink(nullptr);

		Locker dLocker(device);
		device->updateStatus(0);

	}
};

#endif /* CALLMOUNTTASK_H_ */
