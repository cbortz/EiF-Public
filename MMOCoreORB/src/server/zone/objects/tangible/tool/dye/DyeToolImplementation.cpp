/*
 * DyeToolImplementation.cpp
 *
 *  Last updated: Mar 20, 2022
 *      Author: Phoenix
 */

#include "server/zone/objects/tangible/tool/dye/DyeTool.h"
#include "templates/tangible/tool/DyeToolTemplate.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/manufactureschematic/craftingvalues/CraftingValues.h"
#include "server/zone/packets/scene/AttributeListMessage.h"
//#include "server/zone/objects/player/sui/listbox/SuiListBox.h"
//#include "server/zone/objects/player/sui/callbacks/DyeToolSuiCallback.h"

void DyeToolImplementation::loadTemplateData(SharedObjectTemplate* templateData) {
	TangibleObjectImplementation::loadTemplateData(templateData);
}

void DyeToolImplementation::fillAttributeList(AttributeListMessage* msg, CreatureObject* object) {
	TangibleObjectImplementation::fillAttributeList(msg, object);
	// msg->insertAttribute("quality", Math::getPrecision(quality, 1));
}

void DyeToolImplementation::updateCraftingValues(CraftingValues* values, bool firstUpdate) {
	// quality = values->getCurrentValue("quality");
}
int DyeToolImplementation::handleObjectMenuSelect(CreatureObject* player, byte selectedID) {
	return TangibleObjectImplementation::handleObjectMenuSelect(player, selectedID);
}

void DyeToolImplementation::sendDyeListTo(CreatureObject* player) {
}
