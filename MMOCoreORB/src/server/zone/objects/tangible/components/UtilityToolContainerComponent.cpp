/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.
*/

#include "UtilityToolContainerComponent.h"
#include "server/zone/objects/scene/SceneObject.h"
#include "server/zone/objects/creature/CreatureObject.h"

int UtilityToolContainerComponent::canAddObject(SceneObject* sceneObject, SceneObject* object, int containmentType, String& errorDescription) const {

	ManagedReference<SceneObject*> p = sceneObject->getParent().get();

	if (p != nullptr){
		int containment = p->getContainmentType();

		if (containment == 4) {
			errorDescription = "You cannot change datacards while holding the binoculars.";
			return TransferErrorCode::INVALIDTYPE;
		}
	}

	if (!object->isDatacard()) {
			errorDescription = "You can only insert datacards this device.";
			return TransferErrorCode::INVALIDTYPE;
	}

	return 0;
}

bool UtilityToolContainerComponent::checkContainerPermission(SceneObject* sceneObject, CreatureObject* creature, uint16 permission) const {

	return ContainerComponent::checkContainerPermission(sceneObject, creature, permission);
}


/**
 * Is called when this object has been inserted with an object
 * @param object object that has been inserted
 */
int UtilityToolContainerComponent::notifyObjectInserted(SceneObject* sceneObject, SceneObject* object) const {

	return sceneObject->notifyObjectInserted(object);
}

/**
 * Is called when an object was removed
 * @param object object that has been inserted
 */
int UtilityToolContainerComponent::notifyObjectRemoved(SceneObject* sceneObject, SceneObject* object, SceneObject* destination) const {

	return sceneObject->notifyObjectRemoved(object);
}
