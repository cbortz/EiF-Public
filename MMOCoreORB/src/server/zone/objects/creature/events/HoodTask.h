/*
 * HoodTask.h
 *
 *  Created on: 12/09/2022
 *      Author: Halyn
 */

#ifndef HOODTASK_H_
#define HOODTASK_H_

#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/tangible/TangibleObject.h"

class HoodTask : public Task {
	ManagedReference<CreatureObject*> creature;
	ManagedReference<TangibleObject*> hoodedObject;
public:
	HoodTask(CreatureObject* creo, TangibleObject* hoodedClothing) {
		creature = creo;
		hoodedObject = hoodedClothing;
	}

	void run() {
		Reference<Task*> pendingTask = creature->getPendingTask("hoodUp");
		if (pendingTask != nullptr) {
			Locker locker(creature);
			String hoodTemplate = hoodedObject->getHoodedAppearance();
			String hoodCustomization;
			hoodedObject->getCustomizationString(hoodCustomization);
			ManagedReference<SceneObject*> hood = creature->getZoneServer()->createObject(hoodTemplate.hashCode(), 0);

			TangibleObject* tanoHood = cast<TangibleObject*>( hood.get());

			Locker clocker(tanoHood, creature);
			tanoHood->setContainerDenyPermission("owner", ContainerPermissions::MOVECONTAINER);
			tanoHood->setContainerDefaultDenyPermission(ContainerPermissions::MOVECONTAINER);
			tanoHood->setCustomizationString(hoodCustomization);

			ManagedReference<CreatureObject*> strongCreo = creature;
			ManagedReference<TangibleObject*> strongHood = tanoHood;
			Core::getTaskManager()->scheduleTask([strongCreo, strongHood]{
				Locker locker(strongCreo);
				Locker cLocker(strongCreo, strongHood);
				strongCreo->transferObject(strongHood, 4);
				strongCreo->broadcastObject(strongHood, true);
			}, "TransferHoodTask", 100);

			creature->removePendingTask("hoodUp");
		}
		pendingTask = creature->getPendingTask("hoodDown");
		if (pendingTask != nullptr) {
			Locker locker(creature);
			Locker clocker(hoodedObject, creature);
			hoodedObject->destroyObjectFromWorld(true);
			hoodedObject->destroyObjectFromDatabase(true);
			creature->removePendingTask("hoodDown");
		}
	}
};

#endif /* ANIMATIONTASK_H_ */
