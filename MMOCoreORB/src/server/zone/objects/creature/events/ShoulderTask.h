/*
 * ShoulderTask.h
 *
 *  Created on: 10/28/2021
 * 
 */

#ifndef SHOULDERTASK_H_
#define SHOULDERTASK_H_

#include "server/zone/objects/creature/CreatureObject.h"

class ShoulderTask : public Task {
	ManagedReference<CreatureObject*> creature;
	ManagedReference<TangibleObject*> utility;
public:
	ShoulderTask(CreatureObject* creo, TangibleObject* heldUtility) {
		creature = creo;
		utility = heldUtility;
	}

	void run() {
		Reference<Task*> pendingTask = creature->getPendingTask("shoulder");
		if (pendingTask != nullptr) {
			Locker locker(creature);
			utility->stowObject(creature);
			creature->removePendingTask("shoulder");
		}
		pendingTask = creature->getPendingTask("unshoulder");
		if (pendingTask != nullptr) {
			Locker locker(creature);
			utility->unstowObject(creature);
			creature->removePendingTask("unshoulder");
		}
	}
};

#endif /* SHOULDERTASK_H_ */
