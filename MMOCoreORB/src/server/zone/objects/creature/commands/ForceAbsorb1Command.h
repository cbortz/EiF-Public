/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEABSORB1COMMAND_H_
#define FORCEABSORB1COMMAND_H_

#include "QueueCommand.h"

class ForceAbsorb1Command : public JediQueueCommand {
public:

	ForceAbsorb1Command(const String& name, ZoneProcessServer* server) : JediQueueCommand(name, server) {
		buffCRC = BuffCRC::JEDI_FORCE_ABSORB_1;
		blockingCRCs.add(BuffCRC::JEDI_AVOID_INCAPACITATION);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_SHIELD_1);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_ABSORB_2);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_FEEDBACK_1);
		singleUseEventTypes.add(ObserverEventType::FORCEABSORB);
		// Skill mods.
		skillMods.put("force_absorb", 75);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const override {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->checkCooldownRecovery("JEDI_FORCE_ABSORB_1")) {
			creature->sendSystemMessage("You are still recovering and cannot create an absorb shield right now."); //You cannot burst run right now.
			return false;
		} else if ( creature->getHAM(6) < 2000 ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return INVALIDPARAMETERS;
		} else {
			int res =  doJediSelfBuffCommand(creature);
			if (res != SUCCESS) {
				return res;
			}
			int forceAlignment = creature->getForceAlignment();
			forceAlignment += 3;
			creature->setForceAlignment(forceAlignment);
			creature->updateCooldownTimer("JEDI_FORCE_ABSORB_1", (duration * 1000)+30000);
			return SUCCESS;
		}
	}

	void handleBuff(SceneObject* creature, ManagedObject* object, int64 param) const override {
		ManagedReference<CreatureObject*> player = creature->asCreatureObject();

		if (player == nullptr) {
			return;
		}

		ManagedReference<PlayerObject*> ghost = player->getPlayerObject();

		if (ghost == nullptr) {
			return;
		}

		// Client Effect upon hit (needed)
		player->playEffect("clienteffect/pl_force_absorb_hit.cef", "");

		int forceAbsorbed = param;
		
		Buff* buff = player->getBuff(BuffCRC::JEDI_FORCE_ABSORB_1);

		Locker locker(buff, creature);

		ghost->setForcePower(ghost->getForcePower() + param);

		CombatManager::instance()->sendMitigationCombatSpam(player, nullptr, forceAbsorbed, CombatManager::FORCEABSORB);
	}
};

#endif //FORCEABSORB1COMMAND_H_
