/*
				Copyright <Empre in Flames>
		See file COPYING for copying conditions.*/

#ifndef FORCELIGHTNINGBLASTCOMMAND_H_
#define FORCELIGHTNINGBLASTCOMMAND_H_

#include "ForcePowersQueueCommand.h"

class ForceLightningBlastCommand : public ForcePowersQueueCommand {
public:

	ForceLightningBlastCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		const int MAX_ENERGY_LEVEL = 25;

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);

		if (targetObject == nullptr || !(targetObject->isCreatureObject() || targetObject->isTangibleObject()) ) {
			return INVALIDTARGET;
		}

		if( targetObject->isCreatureObject() ){
			CreatureObject* creatureTarget = targetObject.castTo<CreatureObject*>();
			if (!creatureTarget->isAttackableBy(creature)){
				return INVALIDTARGET;
			}
		}

		if( targetObject->isTangibleObject() ){
			TangibleObject* targetTano = targetObject.castTo<TangibleObject*>();
			if (!targetTano->isAttackableBy(creature)){
				return INVALIDTARGET;
			}
		}

		if ( creature->getHAM(6) < 4000 ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return INVALIDPARAMETERS;
		}
		

		if ( creature->hasBuff(STRING_HASHCODE("cracklingEnergy"))){
			Buff* buff = creature->getBuff(STRING_HASHCODE("cracklingEnergy"));
			int energyLevel = buff -> getSkillModifierValue("crackling_energy");
			if (energyLevel == MAX_ENERGY_LEVEL){
				int res = doCombatAction(creature, target);
				if (res == SUCCESS) {
					creature->removeBuff(STRING_HASHCODE("cracklingEnergy"));
					creature->removeBuff(BuffCRC::JEDI_CRACKLING_ENERGY_5);
					int forceAlignment = creature->getForceAlignment();
					forceAlignment -= 15;
					creature->setForceAlignment(forceAlignment);
				}
				return res;
			} else {
                creature->sendSystemMessage("You do not have enough darkside energy for this attack!");
				return INVALIDPARAMETERS;
			}
		} else {
            creature->sendSystemMessage("You do not have any darkside energy for this attack!");
			return INVALIDPARAMETERS;
		}

	}
};

#endif //FORCELIGHTNINGBLASTCOMMAND_H_

