/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#include "ForceHealQueueCommand.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/managers/stringid/StringIdManager.h"
#include "server/zone/managers/collision/CollisionManager.h"
#include "server/zone/managers/frs/FrsManager.h"
#include "server/zone/objects/building/BuildingObject.h"
#include "server/zone/objects/creature/events/InjuryTreatmentTask.h"
#include "server/zone/objects/creature/buffs/DelayedBuff.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/objects/player/FactionStatus.h"

ForceHealQueueCommand::ForceHealQueueCommand(const String& name, ZoneProcessServer* server) : JediQueueCommand(name, server) {
	speed = 3;
	allowedTarget = TARGET_AUTO;

	forceCost = 0;
	forceCostMultiplier = 0;

	statesToHeal = 0;
	healStateCost = 0;

	healBleedingCost = 0;
	healPoisonCost = 0;
	healDiseaseCost = 0;
	healFireCost = 0;

	attributesToHeal = 0;
	woundAttributesToHeal = 0;

	healBattleFatigue = 0;
	healAmount = 0;
	healWoundAmount = 0;

	bleedHealIterations = 1;
	poisonHealIterations = 1;
	diseaseHealIterations = 1;
	fireHealIterations = 1;

	visMod = 10;

	range = 0;

	clientEffect = "clienteffect/pl_force_heal_self.cef";
	animationCRC = STRING_HASHCODE("force_healing_1");
}

int ForceHealQueueCommand::runCommand(CreatureObject* creature, CreatureObject* targetCreature) const {
	ManagedReference<PlayerObject*> playerObject = creature->getPlayerObject();

	if (playerObject == nullptr)
		return GENERALERROR;

	int currentForce = playerObject->getForcePower();
	int totalCost = forceCost;
	bool healPerformed = false;
	int forceAlignment = creature->getForceAlignment();
	bool usedInjuryTreatment = false;

	bool selfHeal = creature->getObjectID() == targetCreature->getObjectID();

	// Attribute Wound Healing
	for (int i = 0; i < 3; i++) {
		// Attrib Values: Health = 1, Action = 2, Mind = 4
		if (woundAttributesToHeal & (1 << i)) {
			for (int j = 0; j < 3; j++) {
				if (totalCost < currentForce) {
					uint8 attrib = (i * 3) + j;
					int woundAmount = targetCreature->getWounds(attrib);

					if (healWoundAmount > 0 && woundAmount > healWoundAmount)
						woundAmount = healWoundAmount;

					totalCost += woundAmount * forceCostMultiplier;

					if (totalCost > currentForce) {
						int forceDiff = totalCost - currentForce;
						totalCost -= forceDiff;
						woundAmount -= forceDiff / forceCostMultiplier;
					}

					if (woundAmount > 0) {
						targetCreature->healWound(creature, attrib, woundAmount, true);
						healPerformed = true;
						sendHealMessage(creature, targetCreature, HEAL_WOUNDS, attrib, woundAmount);
					}
				}
			}
		}
	}


	// HAM Attribute Healing
	if (creature->canTreatInjuries()) {	
		// Check to see if they have enough Mind for a heal
		if ( creature->isPlayerCreature() && !selfHeal && creature->getHAM(6) < 4000 ){
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return GENERALERROR;
		} else if ( creature->isPlayerCreature() && selfHeal && creature->getHAM(6) < 3000  ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return GENERALERROR;
		}

		for (int i = 0; i < 3; i++) {
			// Attrib Values: Health = 1, Action = 2, Mind = 4
			if (attributesToHeal & (1 << i)) {
				if (totalCost < currentForce) {

					uint8 attrib = i * 3;
					int curHam = targetCreature->getHAM(attrib);
					int maxHam = targetCreature->getMaxHAM(attrib) - targetCreature->getWounds(attrib);
					int amtToHeal = maxHam - curHam;
					int modifiedHealAmount = healAmount;
					float alignmentCostMultplier = 1;
					
					// account for medUse and treatment
					int medicineUse = creature->getSkillMod("healing_ability");
					int treatment = creature->getSkillMod("healing_injury_treatment");
					modifiedHealAmount += medicineUse * 20;
					modifiedHealAmount += modifiedHealAmount * treatment / 100;
					if ( forceAlignment > 0){
						modifiedHealAmount +=  modifiedHealAmount * lightAlignBuffModifier * forceAlignment/10000.0f;
						
					} else {
						modifiedHealAmount += Math::max(modifiedHealAmount * darkAlignBuffModifier * forceAlignment/10000.0f, modifiedHealAmount * -0.5f);
					}

					// Battle Fatigue
					float bfScale = targetCreature->calculateBFRatio();
					modifiedHealAmount *= bfScale;

					if (creature->isPlayerCreature()) {
						PlayerManager* playerManager = server->getZoneServer()->getPlayerManager();
						playerManager->sendBattleFatigueMessage(creature, targetCreature);
					}


					if (modifiedHealAmount > 0 && amtToHeal > modifiedHealAmount)
						amtToHeal = modifiedHealAmount;

					totalCost += amtToHeal * forceCostMultiplier;

					if (totalCost > currentForce) {
						int forceDiff = totalCost - currentForce;
						totalCost -= forceDiff;
						amtToHeal -= forceDiff / forceCostMultiplier;
					}

					if (amtToHeal > 0) {
						//creature->sendSystemMessage("Mod Heal Cost Amount: " + String::valueOf(totalCost) + ", player's current force cost multip: " + String::valueOf(forceCostMultiplier));
						targetCreature->healDamage(creature, attrib, amtToHeal, true);
						healPerformed = true;
						usedInjuryTreatment = true;
						sendHealMessage(creature, targetCreature, HEAL_DAMAGE, attrib, amtToHeal);
					}
				}
			}
		}
	}

	// Battle fatigue
	if (totalCost < currentForce && healBattleFatigue != 0) {
		int battleFatigue = targetCreature->getShockWounds();

		if (healBattleFatigue > 0 && battleFatigue > healBattleFatigue)
			battleFatigue = healBattleFatigue;

		totalCost += battleFatigue * forceCostMultiplier;

		if (totalCost > currentForce) {
			int forceDiff = totalCost - currentForce;
			totalCost -= forceDiff;
			battleFatigue -= forceDiff / forceCostMultiplier;
		}

		if (battleFatigue > 0) {
			targetCreature->addShockWounds(-battleFatigue, true, false);
			sendHealMessage(creature, targetCreature, HEAL_FATIGUE, 0, battleFatigue);
			healPerformed = true;
		}
	}

	// States - Stun, Blind, Dizzy, Intim
	if (totalCost < currentForce) {
		int totalStates = 0;
		int healedStates = 0;
		for (int i = 12; i <= 15; i++) {
			int state = (1 << i);

			if ((statesToHeal & state) && targetCreature->hasState(state)) {
				totalStates++;
				int newTotal = totalCost + healStateCost;

				if (newTotal < currentForce) {
					targetCreature->removeStateBuff(state);
					totalCost = newTotal;
					healPerformed = true;
					healedStates++;
				}
			}
		}

		if (healedStates > 0 && healedStates == totalStates)
			sendHealMessage(creature, targetCreature, HEAL_STATES, 0, 0);
	}

	// Bleeding
	if (targetCreature->isBleeding() && healBleedingCost > 0 && totalCost + healBleedingCost < currentForce) {
		bool result = false;
		int iteration = 1;

		while (!result && (totalCost + healBleedingCost < currentForce) && (bleedHealIterations == -1 || iteration <= bleedHealIterations)) {
			result = targetCreature->healDot(CreatureState::BLEEDING, 250, false);
			totalCost += healBleedingCost;
			iteration++;
		}

		if (result) {
			sendHealMessage(creature, targetCreature, HEAL_BLEEDING, 0, 1);
		} else {
			sendHealMessage(creature, targetCreature, HEAL_BLEEDING, 0, 0);
		}

		healPerformed = true;
	}

	// Poison
	if (targetCreature->isPoisoned() && healPoisonCost > 0 && totalCost + healPoisonCost < currentForce) {
		bool result = false;
		int iteration = 1;

		while (!result && (totalCost + healPoisonCost < currentForce) && (poisonHealIterations == -1 || iteration <= poisonHealIterations)) {
			result = targetCreature->healDot(CreatureState::POISONED, 250, false);
			totalCost += healPoisonCost;
			iteration++;
		}

		if (result) {
			sendHealMessage(creature, targetCreature, HEAL_POISON, 0, 1);
		} else {
			sendHealMessage(creature, targetCreature, HEAL_POISON, 0, 0);
		}
	}

	// Disease
	if (targetCreature->isDiseased() && healDiseaseCost > 0 && totalCost + healDiseaseCost < currentForce) {
		bool result = false;
		int iteration = 1;

		while (!result && (totalCost + healDiseaseCost < currentForce) && (diseaseHealIterations == -1 || iteration <= diseaseHealIterations)) {
			result = targetCreature->healDot(CreatureState::DISEASED, 200, false);
			totalCost += healDiseaseCost;
			iteration++;
		}

		if (result) {
			sendHealMessage(creature, targetCreature, HEAL_DISEASE, 0, 1);
		} else {
			sendHealMessage(creature, targetCreature, HEAL_DISEASE, 0, 0);
		}

		healPerformed = true;
	}

	// Fire
	if (targetCreature->isOnFire() && healFireCost > 0 && totalCost + healFireCost < currentForce) {
		bool result = false;
		int iteration = 1;

		while (!result && (totalCost + healFireCost < currentForce) && (fireHealIterations == -1 || iteration <= fireHealIterations)) {
			result = targetCreature->healDot(CreatureState::ONFIRE, 500, false);
			totalCost += healFireCost;
			iteration++;
		}

		if (result) {
			sendHealMessage(creature, targetCreature, HEAL_FIRE, 0, 1);
		} else {
			sendHealMessage(creature, targetCreature, HEAL_FIRE, 0, 0);
		}

		healPerformed = true;
	}

	

	if (healPerformed) {
		
		if (usedInjuryTreatment){
			float modSkill = (float)creature->getSkillMod("healing_injury_speed");

			int delay = (int)round(20.0f - (modSkill / 5));

			//Force the delay to be at least 5 seconds.
			delay = (delay < 5) ? 5 : delay;

			StringIdChatParameter message("healing_response", "healing_response_58"); //You are now ready to heal more damage.
			Reference<InjuryTreatmentTask*> task = new InjuryTreatmentTask(creature, message, "injuryTreatment");
			creature->addPendingTask("injuryTreatment", task, delay * 1000);
		}

		if (selfHeal) {
			creature->playEffect(clientEffect, "");
			creature->setForceAlignment(forceAlignment + 2);
		}
		else {
			creature->doCombatAnimation(targetCreature, animationCRC, 0, 0xFF);
			creature->setForceAlignment(forceAlignment + 5);
		}

		if (currentForce < totalCost) {
			playerObject->setForcePower(0);
			creature->error("Did not have enough force to pay for the healing he did. Total cost of command: " + String::valueOf(totalCost) + ", player's current force: " + String::valueOf(currentForce));
		} else {
			playerObject->setForcePower(currentForce - totalCost);
		}

		VisibilityManager::instance()->increaseVisibility(creature, visMod);

		if (!selfHeal)
			checkForTef(creature, targetCreature);

		return SUCCESS;
	} else {
		if (!creature->canTreatInjuries()){
			creature->sendSystemMessage("@healing_response:healing_must_wait"); //You must wait before you can do that.
		} else {
			if (selfHeal) {
				creature->sendSystemMessage("@jedi_spam:no_damage_heal_self");
			} else {
				creature->sendSystemMessage("@jedi_spam:no_damage_heal_other");
			}
		}

		return GENERALERROR;
	}
}

void ForceHealQueueCommand::sendHealMessage(CreatureObject* creature, CreatureObject* target, int healType, int healSpec, int amount) const {
	if (creature == nullptr || target == nullptr || amount < 0)
		return;

	uint64 playerID = creature->getObjectID();
	uint64 targetID = target->getObjectID();
	const bool selfHeal = playerID == targetID;

	if (healType == HEAL_DAMAGE || healType == HEAL_WOUNDS || healType == HEAL_FATIGUE) {
		String strVal = "@jedi_spam:";

		switch (healType) {
		case HEAL_DAMAGE: strVal = strVal + CreatureAttribute::getName(healSpec) + "_damage"; break;
		case HEAL_WOUNDS: strVal = strVal + CreatureAttribute::getName(healSpec) + "_wounds"; break;
		default: strVal = strVal + "battle_fatigue";
		}

		String statStr = StringIdManager::instance()->getStringId(strVal.hashCode()).toString();

		if (selfHeal) {
			StringIdChatParameter message("jedi_spam", "heal_self");
			message.setTO(statStr);
			message.setDI(amount);
			creature->sendSystemMessage(message);
		} else {
			StringIdChatParameter message("jedi_spam", "heal_other_self");
			message.setTO(statStr);
			message.setDI(amount);
			message.setTT(targetID);
			creature->sendSystemMessage(message);

			if (target->isPlayerCreature()) {
				StringIdChatParameter message("jedi_spam", "heal_other_other");
				message.setTO(statStr);
				message.setDI(amount);
				message.setTT(playerID);
				target->sendSystemMessage(message);
			}
		}
	} else if (healType == HEAL_STATES && amount == 0 && !selfHeal) {
		StringIdChatParameter message("jedi_spam", "stop_states_other");
		message.setTT(targetID);
		creature->sendSystemMessage(message);
	} else if (healType == HEAL_POISON && !selfHeal) {
		if (amount == 1) {
			StringIdChatParameter message("jedi_spam", "stop_poison_other");
			message.setTT(targetID);
			creature->sendSystemMessage(message);
		} else {
			StringIdChatParameter message("jedi_spam", "staunch_poison_other");
			message.setTT(targetID);
			creature->sendSystemMessage(message);
		}
	} else if (healType == HEAL_DISEASE && !selfHeal) {
		if (amount == 1) {
			StringIdChatParameter message("jedi_spam", "stop_disease_other");
			message.setTT(targetID);
			creature->sendSystemMessage(message);
		} else {
			StringIdChatParameter message("jedi_spam", "staunch_disease_other");
			message.setTT(targetID);
			creature->sendSystemMessage(message);
		}
	} else if (healType == HEAL_BLEEDING && !selfHeal) {
		if (amount == 1) {
			StringIdChatParameter message("jedi_spam", "stop_bleeding_other");
			message.setTT(targetID);
			creature->sendSystemMessage(message);
		} else {
			StringIdChatParameter message("jedi_spam", "staunch_bleeding_other");
			message.setTT(targetID);
			creature->sendSystemMessage(message);
		}
	}

	if (amount == 0) {
		if (healType == HEAL_POISON) {
			target->getDamageOverTimeList()->sendDecreaseMessage(target, CreatureState::POISONED);
		} else if (healType == HEAL_DISEASE) {
			target->getDamageOverTimeList()->sendDecreaseMessage(target, CreatureState::DISEASED);
		} else if (healType == HEAL_BLEEDING) {
			target->getDamageOverTimeList()->sendDecreaseMessage(target, CreatureState::BLEEDING);
		} else if (healType == HEAL_FIRE) {
			target->getDamageOverTimeList()->sendDecreaseMessage(target, CreatureState::ONFIRE);
		}
	}
}

int ForceHealQueueCommand::runCommandWithTarget(CreatureObject* creature, CreatureObject* targetCreature) const {
	if (creature == nullptr || targetCreature == nullptr)
		return GENERALERROR;

	if (creature->getObjectID() == targetCreature->getObjectID()) // no self healing
		return GENERALERROR;

	if ((!targetCreature->isPlayerCreature() && !targetCreature->isPet()) || targetCreature->isDroidObject() || targetCreature->isWalkerSpecies())
		return INVALIDTARGET;

	if (targetCreature->isDead() || targetCreature->isAttackableBy(creature))
		return GENERALERROR;

	Locker crossLocker(targetCreature, creature);

	if (creature->isKnockedDown())
		return GENERALERROR;

	if(!checkDistance(creature, targetCreature, range))
		return TOOFAR;

	if (checkForArenaDuel(targetCreature)) {
		creature->sendSystemMessage("@jedi_spam:no_help_target"); // You are not permitted to help that target.
		return GENERALERROR;
	}

	if (!targetCreature->isHealableBy(creature)) {
		creature->sendSystemMessage("@healing:pvp_no_help"); // It would be unwise to help such a patient.
		return GENERALERROR;
	}

	if (!CollisionManager::checkLineOfSight(creature, targetCreature)) {
		creature->sendSystemMessage("@healing:no_line_of_sight"); // You cannot see your target.
		return GENERALERROR;
	}

	if (!playerEntryCheck(creature, targetCreature)) {
		return GENERALERROR;
	}

	return runCommand(creature, targetCreature);
}

int ForceHealQueueCommand::doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
	if (creature == nullptr || !creature->isPlayerCreature())
		return GENERALERROR;

	if (!checkInvalidLocomotions(creature))
		return INVALIDLOCOMOTION;

	int comResult = doCommonJediSelfChecks(creature);

	if (comResult != SUCCESS)
		return comResult;

	ManagedReference<CreatureObject*> targetCreature = nullptr;
	bool isRemoteHeal = range > 0 && ((allowedTarget == TARGET_AUTO) || (allowedTarget & TARGET_OTHER));

	if (isRemoteHeal && target != 0 && target != creature->getObjectID()) {
		ManagedReference<SceneObject*> sceno = server->getZoneServer()->getObject(target);

		if (sceno != nullptr && sceno->isCreatureObject()) {
			targetCreature = sceno.castTo<CreatureObject*>();
		}
	}

	const bool selfHealingAllowed = (allowedTarget & TARGET_SELF) || !isRemoteHeal;
	if (!isRemoteHeal || (targetCreature == nullptr && selfHealingAllowed) || (targetCreature != nullptr && (!targetCreature->isHealableBy(creature)) && selfHealingAllowed)) {
		isRemoteHeal = false;
		targetCreature = creature;
	}

	if (targetCreature == nullptr)
		return GENERALERROR;

	int retval = GENERALERROR;

	if (isRemoteHeal)
		retval = runCommandWithTarget(creature, targetCreature);
	else
		retval = runCommand(creature, targetCreature);

	return retval;
}
