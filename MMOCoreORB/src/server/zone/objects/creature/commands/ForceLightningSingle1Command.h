/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCELIGHTNINGSINGLE1COMMAND_H_
#define FORCELIGHTNINGSINGLE1COMMAND_H_

#include "ForcePowersQueueCommand.h"

class ForceLightningSingle1Command : public ForcePowersQueueCommand {
public:

	ForceLightningSingle1Command(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}



	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {


		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if ( creature->getHAM(6) < 1500 ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return INVALIDPARAMETERS;
		}

		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);

		if (targetObject == nullptr || !(targetObject->isCreatureObject() || targetObject->isTangibleObject()) ) {
			return INVALIDTARGET;
		}

		if( targetObject->isCreatureObject() ){
			CreatureObject* creatureTarget = targetObject.castTo<CreatureObject*>();
			if (!creatureTarget->isAttackableBy(creature)){
				return INVALIDTARGET;
			}
		}

		if( targetObject->isTangibleObject() ){
			TangibleObject* targetTano = targetObject.castTo<TangibleObject*>();
			if (!targetTano->isAttackableBy(creature)){
				return INVALIDTARGET;
			}
		}
		
		float lightningLevel = 1.0f;

		if ( creature->hasBuff(STRING_HASHCODE("cracklingEnergy"))){
			Buff* buff = creature->getBuff(STRING_HASHCODE("cracklingEnergy"));
			int energyLevel = buff -> getSkillModifierValue("crackling_energy");
			lightningLevel += energyLevel/100.0f;	
		}
		
		int accuracyBonus = 75 + lightningLevel * 100 - 100;
		UnicodeString args = "healthDamageMultiplier=" + String::valueOf(lightningLevel) +"f;accuracyBonus=" + String::valueOf(accuracyBonus);
		int res = doCombatAction(creature, target, args);
		if (res == SUCCESS) {
			handleBuffInterval(creature);
			int forceAlignment = creature->getForceAlignment();
			forceAlignment -= 4;
			creature->setForceAlignment(forceAlignment);
		}
		return res;
	}

	void handleBuffInterval(CreatureObject* creature) const {

		const int MAX_ENERGY_LEVEL = 25;
		const int DURATION = 6;

		if ( creature->hasBuff(STRING_HASHCODE("cracklingEnergy"))){
			Buff* buff = creature->getBuff(STRING_HASHCODE("cracklingEnergy"));
			int energyLevel = buff -> getSkillModifierValue("crackling_energy");
			uint32 displayedBuff = 0;
			switch (energyLevel / 5) {
			case 1:
				creature->removeBuff(BuffCRC::JEDI_CRACKLING_ENERGY_1);
				displayedBuff = BuffCRC::JEDI_CRACKLING_ENERGY_2;
				break;
			case 2:
				creature->removeBuff(BuffCRC::JEDI_CRACKLING_ENERGY_2);
				displayedBuff = BuffCRC::JEDI_CRACKLING_ENERGY_3;
				break;
			case 3:
				creature->removeBuff(BuffCRC::JEDI_CRACKLING_ENERGY_3);
				displayedBuff = BuffCRC::JEDI_CRACKLING_ENERGY_4;
				break;
			case 4:
				creature->removeBuff(BuffCRC::JEDI_CRACKLING_ENERGY_4);
				displayedBuff = BuffCRC::JEDI_CRACKLING_ENERGY_5;
				break;
			case 5:
				creature->removeBuff(BuffCRC::JEDI_CRACKLING_ENERGY_5);
				displayedBuff = BuffCRC::JEDI_CRACKLING_ENERGY_5;
				break;
			}
			
			Locker locker(buff);
			if (energyLevel < MAX_ENERGY_LEVEL){
				buff -> setSkillModifier("crackling_energy", energyLevel + 5);
			}
			buff->renew();
			locker.release();
			ManagedReference<Buff*> displayBuff = new Buff(creature, displayedBuff, DURATION, BuffType::SKILL);
			Locker blocker(displayBuff);
			creature->addBuff(displayBuff);
		} else {
			ManagedReference<Buff*> buff = new Buff(creature, STRING_HASHCODE("cracklingEnergy"), DURATION, BuffType::SKILL);
			Locker locker(buff);
			buff->setSkillModifier("crackling_energy", 5);
			creature->addBuff(buff);
			locker.release();
			ManagedReference<Buff*> displayBuff = new Buff(creature, BuffCRC::JEDI_CRACKLING_ENERGY_1, DURATION, BuffType::SKILL);
			Locker blocker(displayBuff);
			creature->addBuff(displayBuff);
		}
	}

};

#endif //FORCELIGHTNINGSINGLE1COMMAND_H_
