/*
				Copyright EiF
		See file COPYING for copying conditions.*/

#ifndef GUTSHOTCOMMAND_H_
#define GUTSHOTCOMMAND_H_

#include "CombatQueueCommand.h"

class GutShotCommand : public CombatQueueCommand {
public:

	GutShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //GUTSHOTCOMMAND_H_
