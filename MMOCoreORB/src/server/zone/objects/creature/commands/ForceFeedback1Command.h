/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCEFEEDBACK1COMMAND_H_
#define FORCEFEEDBACK1COMMAND_H_

#include "QueueCommand.h"

class ForceFeedback1Command : public JediQueueCommand {
public:

	ForceFeedback1Command(const String& name, ZoneProcessServer* server) : JediQueueCommand(name, server) {
		buffCRC = BuffCRC::JEDI_FORCE_FEEDBACK_1;
		blockingCRCs.add(BuffCRC::JEDI_FORCE_SHIELD_1);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_ABSORB_1);
		blockingCRCs.add(BuffCRC::JEDI_FORCE_FEEDBACK_2);
		singleUseEventTypes.add(ObserverEventType::FORCEFEEDBACK);
		skillMods.put("force_feedback", 65);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->checkCooldownRecovery("JEDI_FORCE_FEEDBACK_1")) {
			creature->sendSystemMessage("You are still recovering and cannot create a reflective shield right now.");
			return false;
		} else if ( creature->getHAM(6) < 4000 ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return INVALIDPARAMETERS;
		} else {
			int res =  doJediSelfBuffCommand(creature);
			if (res != SUCCESS) {
				return res;
			}
			int forceAlignment = creature->getForceAlignment();
			forceAlignment += 2;
			creature->setForceAlignment(forceAlignment);
			creature->updateCooldownTimer("JEDI_FORCE_FEEDBACK_1", (duration * 1000)+25000);
			return SUCCESS;
		}
	}

};

#endif //FORCEFEEDBACK1COMMAND_H_
