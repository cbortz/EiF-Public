/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef INTERCEDECOMMAND_H_
#define INTERCEDECOMMAND_H_

#include "server/zone/objects/scene/SceneObject.h"
#include "JediQueueCommand.h"
#include "QueueCommand.h"

class IntercedeCommand : public JediQueueCommand {
public:

    IntercedeCommand(const String& name, ZoneProcessServer* server) 
		: JediQueueCommand(name, server) {
		
		// BuffCRC's, first one is used.
		buffCRC = STRING_HASHCODE("interceding");
    
		// If these are active they will block buff use
		blockingCRCs.add(BuffCRC::JEDI_AVOID_INCAPACITATION);
    
		// Skill mods.
		skillMods.put("interceding", 1);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		
		
		if (!creature->checkCooldownRecovery("JEDI_INTERCEDE")) {
			creature->sendSystemMessage("You are still recovering and cannot intercede for your allies now."); //You cannot burst run right now.
			return false;
		}

		if ( creature->getHAM(6) < 3000 ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return INVALIDPARAMETERS;
		}

		int res = creature->hasBuff(buffCRC) || creature->hasBuff(STRING_HASHCODE("interceded")) ? NOSTACKJEDIBUFF : doJediSelfBuffCommand(creature);
		
		if (res == NOSTACKJEDIBUFF) {
			creature->sendSystemMessage("You are already interceding or someone in your group is interceding you"); 
			return GENERALERROR;
		}

		if (res != SUCCESS) {
			return res;
		}

		// need to apply the damage reduction in a separate buff so that the multiplication and division applies right
		Buff* buff = creature->getBuff(STRING_HASHCODE("interceding"));
		if (buff == nullptr)
			return GENERALERROR;

		ManagedReference<PrivateSkillMultiplierBuff*> multBuff = new PrivateSkillMultiplierBuff(creature, name.hashCode(), duration, BuffType::JEDI);

		Locker locker(multBuff);

		multBuff->setSkillModifier("private_damage_divisor", 20);

		creature->addBuff(multBuff);

		locker.release();

		Locker blocker(buff);

		buff->addSecondaryBuffCRC(multBuff->getBuffCRC());


		ManagedReference<CreatureObject*> player = cast<CreatureObject*>(creature);
		ManagedReference<GroupObject*> group = player->getGroup();

		doInterceding(player,group, duration);

		// Return if something is in error.
		if (res != SUCCESS) {
			return res;
		} else {
			creature->updateCooldownTimer("JEDI_INTERCEDE", (duration * 1000)+120000);
		}
		
		// Return.
		return SUCCESS;
	}

	bool doInterceding(CreatureObject* leader, GroupObject* group, int duration) const {
		if (leader == nullptr || group == nullptr)
			return false;

		// Use i = 0 for the whole group, i=1 for everyone but the leader.
		for (int i = 0; i < group->getGroupSize(); i++) {
			ManagedReference<CreatureObject*> member = group->getGroupMember(i);

			if (member == nullptr || member->getZone() != leader->getZone() || !member->isInRange(leader, 16.0) || member == leader)
				continue;

			Locker clocker(member, leader);

			if (member != leader)
				member->sendSystemMessage("You are being interceded by a Force user!"); //"Your group rallies to the attack!"

			ManagedReference<Buff*> buff = new Buff(member, STRING_HASHCODE("interceded"), duration, BuffType::SKILL);

			Locker locker(buff);

			buff->setSkillModifier("interceded", 1);

			member->addBuff(buff);
			member->setIntercededBy(leader);

			checkForTef(leader, member);
		}
		return true;
	}

};

#endif //INTERCEDECOMMAND_H_
